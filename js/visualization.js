visualization = function(visId, elementId, comChannel) {
	//TODO: add subvisualizations - multiple lines...

	var vis = {};
	vis.id = visId;
	vis.element = elementId;
	vis.comChannel = comChannel;
	vis.subscriptions = new Array();
	vis.binding = null;

	vis.ylabel = "";

	vis.setBinding = function(binding) {
		vis.binding = binding;
		subscribe(binding);
	};

	function subscribe(binding) {

		var subscription;

		subscription = vis.comChannel.subscribe(binding.query + ".exec", function(data, envelope) {
			vis.data = [];
			data.forEach(function(d) {
				dataItem = {};
				dataItem.origData = d;
				binding.vals.forEach(function(val) {
					dataItem[val.visVar] = d[val.qVar];
				});
				vis.data.push(dataItem);
			});
			vis.plot();
		});
		vis.subscriptions.push(subscription);

		subscription = vis.comChannel.subscribe(binding.query + ".select.*", function(data, envelope) {
			// find the id variable (column)
			var id;
			vis.binding.vals.forEach(function(d) {
				if (d.visVar == "column") {
					id = d.qVar;
				}
			});
			vis.setSelected(data[id]);
		});

	};

	vis.setSelected = function(id) {
		vis.data.forEach(function(d) {
			d.selected = (d.column == id) ? true : false;
		});
		vis.svg.selectAll(".bar").attr("fill", function(d) {
			return (d.selected == true) ? "green" : "steelblue";
		});
	};

	vis.margin = {
		top : 20,
		right : 20,
		bottom : 30,
		left : 40
	}, vis.width = 960 - vis.margin.left - vis.margin.right, vis.height = 500 - vis.margin.top - vis.margin.bottom;

	vis.x = d3.scale.ordinal().rangeRoundBands([0, vis.width], .1);
	vis.y = d3.scale.linear().range([vis.height, 0]);
	vis.xAxis = d3.svg.axis().scale(vis.x).orient("bottom");
	vis.yAxis = d3.svg.axis().scale(vis.y).orient("left").ticks(10, "%");

	vis.plot = function() {
		// remove other visualization in the div
		d3.select(vis.element + " svg").remove();
		vis.svg = d3.select(vis.element).append("svg").attr("width", vis.width + vis.margin.left + vis.margin.right).attr("height", vis.height + vis.margin.top + vis.margin.bottom).append("g").attr("transform", "translate(" + vis.margin.left + "," + vis.margin.top + ")");

		vis.x.domain(vis.data.map(function(d) {
			return d.column;
		}));
		vis.y.domain([0, d3.max(vis.data, function(d) {
			return d.value;
		})]);

		vis.svg.append("g").attr("class", "x axis").attr("transform", "translate(0," + vis.height + ")").call(vis.xAxis);

		vis.svg.append("g").attr("class", "y axis").call(vis.yAxis).append("text").attr("transform", "rotate(-90)").attr("y", 6).attr("dy", ".71em").style("text-anchor", "end").text(vis.ylabel);

		vis.svg.selectAll(".bar").data(vis.data).enter().append("rect").attr("class", "bar").attr("x", function(d) {
			return vis.x(d.column);
		}).attr("width", vis.x.rangeBand()).attr("y", function(d) {
			return vis.y(d.value);
		}).attr("height", function(d) {
			return vis.height - vis.y(d.value);
		}).attr("fill", function(d) {
			if (d.selected) {
				return "green";
			} else {
				return "steelblue";
			}
		}).on("click", function(d) {
			vis.comChannel.publish(vis.binding.query + ".select." + vis.id, d.origData);
		});

	};
	function type(d) {
		d.value = +d.value;
		return d;
	}

	return vis;
};
