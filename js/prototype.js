var tap = postal.addWireTap( function( d, e ) {
    console.log( JSON.stringify( e ) );
});
var viewChanel = postal.channel("visMessages");

var q1 = new query("q1", "Query 1", viewChanel);
q1.data = [{
	frequency : 0.18,
	importance: 5,
	letter : "A"
}, {
	frequency : 0.08,
	importance: 10,
	letter : "B"
}, {
	frequency : 0.02,
	importance: 25,
	letter : "C"
}];

var q2 = new pquery("q2", "Query 2", viewChanel);
q2.data1 = [{
	count : 4920,
	subletter : "A1"
}, {
	count : 5000,
	subletter : "A2"
}, {
	count : 1230,
	subletter : "A3"
}];
q2.data2 = [{
	count : 1220,
	subletter : "B1"
}, {
	count : 3000,
	subletter : "B2"
}, {
	count : 6230,
	subletter : "B3"
}];

var vis1 = new visualization("v1", "#vis1", viewChanel);
var binding1 = {
	query : "q1",
	vals : [{
		qVar : "frequency",
		visVar : "value"
	}, {
		qVar : "letter",
		visVar : "column"
	}]
};
vis1.ylabel = "Frequency";
vis1.setBinding(binding1);
vis2 = new visualization("v2","#vis2", viewChanel);
var binding2 = {
	query : "q1",
	vals : [{
		qVar : "importance",
		visVar : "value"
	}, {
		qVar : "letter",
		visVar : "column"
	}]
};
vis2.setBinding(binding2);
vis2.ylabel = "Importance";
q1.execute();

var vis3 = new visualization("v3", "#vis3", viewChanel);
var binding3 = {
	query : "q2",
	vals : [{
		qVar : "count",
		visVar : "value"
	}, {
		qVar : "subletter",
		visVar : "column"
	}]
};
vis3.ylabel = "Count";
vis3.setBinding(binding3);
q2.execute();



var o = {
	get: function(visVar) {
		
	},
	
};



