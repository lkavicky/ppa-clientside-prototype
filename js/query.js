query = function(id, name, comChannel) {
	//autoExecute boolean property - when true, execute after input params set
	var query = {};

	query.id = id;
	query.name = name;
	query.comChannel = comChannel;
	query.data = {};

	query.setParam = function(param) {
		//check...
	};
	query.execute = function() {
		//execution of the query
		//TODO not having all the parameters bound
		query.comChannel.publish(query.id + ".exec", query.data);
	};
	
	return query;
};

pquery = function(id, name, comChannel) {
	//autoExecute boolean property - when true, execute after input params set
	var query = {};

	query.id = id;
	query.name = name;
	query.comChannel = comChannel;
	query.data = {};
	query.data1 = {};
	query.data2 = {};
	query.param = null;

	postal.subscribe({
		channel : "visMessages",
		topic : "q1.select.*",
		callback : function(data, envelope) {
			query.param = data.letter;
			if (data.letter == "A") {
				query.data = query.data1;
			} else {
				query.data = query.data2;
			}
			query.execute();
		}
	});

	query.execute = function() {
		//execution of the query

		if (query.param == null)
			return false;
		//TODO not having all the parameters bound

		query.comChannel.publish(query.id + ".exec", query.data);

	};
	return query;
};

// parameter as new object